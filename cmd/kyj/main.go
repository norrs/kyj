package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"github.com/ghodss/yaml"
	"github.com/santhosh-tekuri/jsonschema/v2"
	_ "github.com/santhosh-tekuri/jsonschema/v2/httploader"
	"github.com/savaki/jq"
	"io"
	"io/ioutil"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	var raw bool
	flag.BoolVar(&raw, "raw", false, "Set to true to _skip_ assuming filename is a kubernetes yaml file and extract's it's .spec to validate against.")
	var schemaFilename string
	flag.StringVar(&schemaFilename, "schema", "", "Filepath to schema")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n <filename> [filename] [filename]\n", os.Args[0])
		flag.PrintDefaults()
	}

	flag.Parse()
	args := flag.Args()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	registerOpenApi3Formats()
	schema, err := jsonschema.Compile(schemaFilename)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	var r io.ReadCloser

	foundDuringLastDocument := false
	numErrors := 0
	for _, filename := range args {
		if foundDuringLastDocument {
			fmt.Fprintf(os.Stderr, "---\n")
			foundDuringLastDocument = false
		}
		var err error
		if !raw {
			var data, jsonData []byte

			if filename == "-" {
				data, err = ioutil.ReadAll(os.Stdin)
				check(err)
			} else {
				data, err = ioutil.ReadFile(filename)
				check(err)
			}

			jsonData, err = yaml.YAMLToJSON(data)
			op, _ := jq.Parse(".spec")
			specData, _ := op.Apply(jsonData)
			r = ioutil.NopCloser(bytes.NewReader(specData))
		} else if filename == "-" {
			r = ioutil.NopCloser(bufio.NewReader(os.Stdin))
		} else {
			r, err = jsonschema.LoadURL(filename)
			if err != nil {
				fmt.Fprintf(os.Stderr, "error in reading %q. reason: \n%v\n", filename, err)
			}
		}
		if err == nil {
			err = schema.Validate(r)
			_ = r.Close()
			if err != nil {
				fmt.Fprintf(os.Stderr, "%q does not conform to the schema specified. reason:\n%v\n", filename, err)
			}
		}
		if err != nil {
			numErrors = numErrors + 1
			foundDuringLastDocument = true
		}
	}
	os.Exit(numErrors)
}

/**
OpenAPI3 extends the JsonSchema with additional formats.

The value of this keyword is called a format attribute. It MUST be a string.
A format attribute can generally only validate a given set of instance types.
If the type of the instance to validate is not in this set, validation for this format attribute and instance SHOULD succeed.

Hence we cheat and always return true on these Formats we are missing.

See https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#data-types
See https://json-schema.org/latest/json-schema-validation.html#rfc.section.7
*/
func registerOpenApi3Formats() {
	jsonschema.Formats["float"] = isAlwaysTrue
	jsonschema.Formats["double"] = isAlwaysTrue
}

func isAlwaysTrue(interface{}) bool {
	return true
}
