module gitlab.com/zedge-oss/kyj

go 1.12

require (
	github.com/ghodss/yaml v1.0.0
	github.com/santhosh-tekuri/jsonschema/v2 v2.0.1
	github.com/savaki/jq v0.0.0-20161209013833-0e6baecebbf8
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
